# Generated by Django 2.1.7 on 2019-03-20 10:48

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('qeapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExamSetUP',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('exam_title', models.PositiveSmallIntegerField(choices=[(1, 'Primary Job Exam'), (2, 'BCS Job Exam'), (3, 'Bank Job Exam'), (4, 'Others Job Exam')])),
                ('exam_package', models.PositiveSmallIntegerField(choices=[(1, '1 Exam - 50 Tk'), (2, '5 Exam - 200 Tk'), (3, '10 Exam - 250 Tk'), (4, '20 Exam - 300 Tk')])),
                ('exam_time', models.PositiveSmallIntegerField(choices=[(1, '07:00 AM'), (2, '10:00 AM'), (3, '01:00 PM'), (4, '04:00 PM'), (4, '07:00 PM'), (4, '10:00 PM')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exam_setup_creator', to=settings.AUTH_USER_MODEL)),
                ('user', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='examsetup', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bkash_number', models.CharField(blank=True, max_length=20, null=True)),
                ('transaction_id', models.CharField(blank=True, max_length=20, null=True)),
                ('transaction_amount', models.IntegerField()),
                ('security_key', models.CharField(blank=True, max_length=20, null=True)),
                ('user', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='payment', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
