from django.apps import AppConfig


class QeappConfig(AppConfig):
    name = 'qeapp'
