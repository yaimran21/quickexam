import re
from datetime import timezone

from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import User, PermissionsMixin, UserManager
from django.core.mail import send_mail
from django.core.validators import RegexValidator
from django.db import models


# Create your models here.
from rest_framework import validators


class Profile(models.Model):
    country_choice = (
        (1, 'Bangladesh'),
        (2, 'Japan'),
        (3, 'United Kingdom'),
        (4, 'United States'),
    )
    user = models.OneToOneField(User,
                related_name='profile',
                on_delete=models.CASCADE,
                null=True, blank=True)

    name = models.CharField(max_length=300)
    date_of_birth = models.DateField()
    phone = models.CharField(max_length=20)
    city = models.CharField(max_length=50, null=True, blank=True)
    country = models.PositiveSmallIntegerField(choices=country_choice, default=1)
    postal_code = models.CharField(max_length=300, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE,
                related_name='profile_creator')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + ' ' +self.phone

    # @property
    # def full_name(self):
    #     return self.first_name + ' ' +self.last_name
    class Meta:
        db_table = 'profiles'


class ExamSetUP(models.Model):
    exam_title = (
        (1, 'Primary Job Exam'),
        (2, 'BCS Job Exam'),
        (3, 'Bank Job Exam'),
        (4, 'Others Job Exam'),
    )
    exam_package = (
        (1, '1 Exam - 50 Tk'),
        (2, '5 Exam - 200 Tk'),
        (3, '10 Exam - 250 Tk'),
        (4, '20 Exam - 300 Tk'),
    )
    exam_time = (
        (1, '07:00 AM'),
        (2, '10:00 AM'),
        (3, '01:00 PM'),
        (4, '04:00 PM'),
        (4, '07:00 PM'),
        (4, '10:00 PM'),
    )

    user = models.OneToOneField(User,
                related_name='examsetup',
                on_delete=models.CASCADE,
                null=True, blank=True)
    exam_title = models.PositiveSmallIntegerField(choices=exam_title)
    exam_package = models.PositiveSmallIntegerField(choices=exam_package)
    exam_time = models.PositiveSmallIntegerField(choices=exam_time)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE,
                                   related_name='exam_setup_creator')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user + '>>' + self.exam_title + '>>' + self.exam_package + '>>' + self.exam_time

        # @property
        # def full_name(self):
        #     return self.first_name + ' ' +self.last_name

    class Meta:
        db_table = 'examsetups'


class Payment(models.Model):
    user = models.OneToOneField(User,
                                related_name='payment',
                                on_delete=models.CASCADE,
                                null=True, blank=True)
    bkash_number = models.CharField(max_length=20, null=True, blank=True)
    transaction_id = models.CharField(max_length=20, null=True, blank=True)
    transaction_amount = models.IntegerField()
    security_key = models.CharField(max_length=20, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE,
                                   related_name='payment_creator')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user + '>>' + self.bkash_number + '>>' + self.transaction_amount

        # @property
        # def full_name(self):
        #     return self.first_name + ' ' +self.last_name

    class Meta:
        db_table = 'payments'
