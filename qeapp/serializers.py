from django.contrib.auth.models import User
from rest_framework import serializers

from qeapp import models


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Profile
        fields = ('name', 'date_of_birth', 'phone', 'city', 'country','postal_code' )

