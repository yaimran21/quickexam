from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import *


class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'name', 'date_of_birth', 'phone', 'city','country']
    list_filter = ['user', 'name', 'country']


class ExamSetUpAdmin(admin.ModelAdmin):
    list_display = ['user', 'exam_title', 'exam_package', 'exam_time']
    list_filter = ['user', 'exam_title', 'exam_package', 'exam_time']


class PaymentAdmin(admin.ModelAdmin):
    list_display = ['user', 'bkash_number', 'transaction_id', 'transaction_amount']
    list_filter = ['user']


admin.site.register(Profile, ProfileAdmin)
admin.site.register(ExamSetUP, ExamSetUpAdmin)
admin.site.register(Payment, PaymentAdmin)