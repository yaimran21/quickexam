from django.urls import path, include
from api import views

urlpatterns = [
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
    path('profile/', views.ProfileAPIViewSet.as_view()),

]
# app_name = 'api'
