from rest_framework import generics, permissions
from qeapp import models, serializers
# Create your views here.
from qeapp.models import Profile


class ProfileAPIViewSet(generics.ListCreateAPIView):
    # queryset = models.Profile.objects.get(user=request.user)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.ProfileSerializer

    def perform_create(self, serializer):
        queryset = models.Profile.objects.filter(user=self.request.user)
        if queryset.exists():
            raise ValueError('Your profile is already created!')
        serializer.save(user=self.request.user, created_by=self.request.user)

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        user = self.request.user
        queryset = models.Profile.objects.filter(user=user)
        return queryset
